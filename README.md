## Getting Started

This repository contains two separate Maven projects: `service-log` and `service-shuffle`.

Each Maven project have three sub-modules: `api`, `server`, and `service`.

- **api**: This module stores the OpenAPI definition for the service. This can be used for both client and server code generation. Client and server code can be generated for any language.
- **server**: This module is module, where application initiated. We keep `application,properties`  in this module.
- **service**: In this module we keep application logic


## Run with docker

```bash
# Build docker files. Using caching,  first time build may take a long time
docker-compose build

# Run docker-compose
docker-compose up
```
***

# Building Without Docker

First build service-log.
service-shuffle using service-log-api as dependency for generating client code.

1. Building service-log
   ```sh
   cd service-log
   make package
   ```
2. Building service-shuffle
   ```sh
   cd service-shuffle
   make package
   ```

# Running test cases

1. Building service-log
   ```sh
   cd service-log
   make test
   ```
2. Building service-shuffle
   ```sh
   cd service-shuffle
   make test
   ```
You can also test sub-modules:
   ```sh
   cd service-shuffle
   make test_server
   ```

# Directly running
1. Running service-log
   ```sh
   cd service-log
   make run
   ```
2. Running service-shuffle
   ```sh
   cd service-shuffle
   make run
   ```

# Using make instead of mvn

It is better to use make commands. Because modules dependes on each other,
it is necessary to understand which module depends on which module.
By using make commands, you don't need to worry about module dependency.
I created make commands for clean, package, test and deploy.
Check MakeFile for more commands.
