
REQUIRED_JAVA_VERSION = 21

all: check_java_version package_server

run: package
	echo "Starting server"
	cd service-log-server && mvn spring-boot:run -Dspring-boot.run.profiles=$(SPRING_PROFILE)

check_java_version:
	@JAVA_VERSION=$$(java -version 2>&1 | awk -F '"' '/version/ {print $$2}' | awk -F[._] '{print $$1}'); \
	if [ "$$JAVA_VERSION" -lt $(REQUIRED_JAVA_VERSION) ]; then \
		echo "Error: Java version is $$JAVA_VERSION. Java $(REQUIRED_JAVA_VERSION) or higher is required."; \
		exit 1; \
	fi


init_parent:
	echo "Generating parent container without any submodule"
	mvn clean install -N

clean: clean_api clean_service clean_server

clean_service:
	echo "Cleaning service"
	mvn clean -pl service-log-service

clean_server:
	echo "Cleaning server"
	mvn clean -pl service-log-server

clean_api:
	echo "Cleaning api"
	mvn clean -pl service-log-api

test: test_api test_service test_server

test_service: init_parent
	echo "Testing service"
	mvn test -pl service-log-service

test_server: deploy_api deploy_service
	echo "Testing server"
	mvn test -pl service-log-server

test_api: init_parent
	echo "Testing api"
	mvn test -pl service-log-api


package: package_server

package_service: init_parent
	echo "Packaging service"
	mvn package -pl service-log-service

package_server: deploy_api deploy_service
	echo "Packaging server"
	mvn package -pl service-log-server

package_api: init_parent
	echo "Packaging api"
	mvn package -pl service-log-api


deploy: deploy_api deploy_service deploy_server

deploy_service: init_parent
	echo "Deploying artifact for service"
	mvn install -pl service-log-service

deploy_server: init_parent
	echo "Deploying artifact for server"
	mvn install -pl service-log-server

deploy_api: init_parent
	echo "Deploying artifact for api"
	mvn install -pl service-log-api