package org.example.log.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceLogServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServiceLogServerApplication.class, args);
    }
}