package org.example.log.server.controller;


import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.example.log.api.LogApi;
import org.example.log.api.model.LogPost201Response;
import org.example.log.api.model.LogPostRequest;
import org.example.log.service.LogService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
@RequiredArgsConstructor
public class LogController implements LogApi {

    private final LogService logService;

    @Override
    public ResponseEntity<LogPost201Response> logPost(LogPostRequest logPostRequest) {
        log.debug("logPost started");

        logService.logRequest(logPostRequest.getRequest());

        log.debug("logPost ended");
        return new ResponseEntity<>(new LogPost201Response(), HttpStatus.CREATED);
    }
}
