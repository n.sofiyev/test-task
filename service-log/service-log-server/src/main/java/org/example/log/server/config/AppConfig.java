package org.example.log.server.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"org.example.log.server",
        "org.example.log.service"})
public class AppConfig {
}
