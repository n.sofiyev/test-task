package org.example.log.service.impl;

import org.example.log.service.LogService;
import org.springframework.stereotype.Service;

@Service
class LogServiceImpl implements LogService {

    @Override
    public void logRequest(String requestBody) {
        //println is synchronized
        System.out.println(requestBody);
    }
}
