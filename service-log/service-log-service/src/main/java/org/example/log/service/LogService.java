package org.example.log.service;

public interface LogService {

    void logRequest(String requestBody);
}
