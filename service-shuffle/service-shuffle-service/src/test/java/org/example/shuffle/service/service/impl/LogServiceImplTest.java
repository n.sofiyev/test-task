package org.example.shuffle.service.service.impl;

import org.example.log.api.DefaultApi;
import org.example.log.api.model.LogPost201Response;
import org.example.log.api.model.LogPostRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class LogServiceImplTest {

    @Mock
    private DefaultApi defaultApi;

    @Mock
    private ExecutorService logApiClientExecutor;

    @InjectMocks
    private LogServiceImpl logService;

    @BeforeEach
    void setUp() {
        logService = new LogServiceImpl(defaultApi, logApiClientExecutor, 10);
    }

    @Test
    void testApiCalledOneTime() throws Exception {
        String requestBody = "some test body";
        LogPost201Response apiResponse = new LogPost201Response();
        when(defaultApi.logPost(any(LogPostRequest.class))).thenReturn(apiResponse);

        logService.postLogRequest(requestBody);

        ArgumentCaptor<Runnable> captor = ArgumentCaptor.forClass(Runnable.class);
        verify(logApiClientExecutor).submit(captor.capture());
        Runnable task = captor.getValue();

        task.run();

        ArgumentCaptor<LogPostRequest> requestCaptor = ArgumentCaptor.forClass(LogPostRequest.class);
        verify(defaultApi).logPost(requestCaptor.capture());
        LogPostRequest logPostRequest = requestCaptor.getValue();
        assertEquals(requestBody, logPostRequest.getRequest());

        verify(defaultApi, times(1)).logPost(any(LogPostRequest.class));
    }

    @Test
    void testPreDestroy() throws InterruptedException {
        ExecutorService mockExecutorService = mock(ExecutorService.class);
        LogServiceImpl serviceWithInjectedExecutor = new LogServiceImpl(defaultApi, mockExecutorService, 10);

        serviceWithInjectedExecutor.preDestroy();

        verify(mockExecutorService, times(1)).shutdown();
        //As it is mock executor, it will not terminate, so executor will 2 times invoke. Which is perfect scenario for testing
        verify(mockExecutorService, times(2)).awaitTermination(10, TimeUnit.SECONDS);
    }
}
