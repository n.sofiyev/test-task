package org.example.shuffle.service.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.*;

import static org.junit.jupiter.api.Assertions.*;

public class ShuffleServiceTest {

    private ShuffleServiceImpl shuffleService;

    @BeforeEach
    void setUp() {
        shuffleService = new ShuffleServiceImpl();
    }

    @Test
    void testShuffledArraySize() {
        final int expectedSize = 1000;
        final List<Integer> shuffledArray = shuffleService.generateShuffleArray(expectedSize);

        assertEquals(expectedSize, shuffledArray.size(), "The size of the generated array should be equal to the given size.");
    }

    @Test
    void testShuffledArrayContainsAllUniqueElements() {
        final int size = 1000;
        final List<Integer> shuffledArray = shuffleService.generateShuffleArray(size);
        final Set<Integer> expectedSet = new HashSet<>();

        for (int i = 1; i <= size; i++) {
            expectedSet.add(i);
        }

        assertTrue(expectedSet.containsAll(shuffledArray), "The generated array should contain all elements from 1 to size.");
    }

    @Test
    void testShuffledArrayIsShuffled() {
        final int size = 1000;
        final List<Integer> shuffledArray = shuffleService.generateShuffleArray(size);

        boolean isShuffled = false;
        for (int i = 0; i < shuffledArray.size() - 1; i++) {
            if (shuffledArray.get(i) > shuffledArray.get(i + 1)) {
                isShuffled = true;
                break;
            }
        }
        assertTrue(isShuffled, "The generated array should be shuffled.");
    }

    @Test
    void testShuffledArrayRandomness() {
        final int size = 10;
        final List<Integer> shuffledArray1 = shuffleService.generateShuffleArray(size);
        final List<Integer> shuffledArray2 = shuffleService.generateShuffleArray(size);

        boolean isDifferent = false;
        for (int i = 0; i < size; i++) {
            if (!shuffledArray1.get(i).equals(shuffledArray2.get(i))) {
                isDifferent = true;
                break;
            }
        }

        assertTrue(isDifferent, "The generated array should be different on subsequent calls.");
    }

    @Test
    void testShuffledArrayRandomnessUsingDifferentTheads() throws InterruptedException, ExecutionException {
        final int size = 100;
        final int concurrency = 20;

        try (ExecutorService executorService = Executors.newFixedThreadPool(concurrency)) {
            Callable<List<Integer>> task = () -> shuffleService.generateShuffleArray(size);

            List<Future<List<Integer>>> results = new ArrayList<>();
            for (int i = 0; i < concurrency; i++) {
                results.add(executorService.submit(task));
            }

            List<List<Integer>> generatedArrays = new ArrayList<>();
            for (Future<List<Integer>> future : results) {
                generatedArrays.add(future.get());
            }

            for (int i = 0; i < generatedArrays.size(); i++) {
                for (int j = i + 1; j < generatedArrays.size(); j++) {
                    assertNotEqualsLists(generatedArrays.get(i), generatedArrays.get(j), "Each shuffled array should be unique.");
                }
            }
            executorService.shutdown();
        }
    }

    private void assertNotEqualsLists(final List<Integer> list1,
                                      final List<Integer> list2,
                                      final String message) {
        assertEquals(list1.size(), list2.size(), message);

        boolean isDifferent = false;
        for (int i = 0; i < list1.size(); ++i) {
            if (!list1.get(i).equals(list2.get(i))) {
                isDifferent = true;
                break;
            }
        }

        assertTrue(isDifferent, message);
    }
}
