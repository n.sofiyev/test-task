package org.example.shuffle.service.executor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
class LogApiClientExecutorConf {

    @Bean
    ExecutorService getLogApiClientExecutor() {
        return Executors.newVirtualThreadPerTaskExecutor();
    }
}
