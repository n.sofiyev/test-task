package org.example.shuffle.service.client;

import org.example.log.api.DefaultApi;
import org.example.log.invoker.ApiClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class AsyncApiClientConf {

    @Value("${service.log.url}")
    private String logServiceUrl;

    @Bean
    DefaultApi createApi() {
        return new DefaultApi(createApiClient());
    }

    ApiClient createApiClient() {
        ApiClient apiClient = new ApiClient();
        apiClient.setHost(logServiceUrl);
        return apiClient;
    }
}
