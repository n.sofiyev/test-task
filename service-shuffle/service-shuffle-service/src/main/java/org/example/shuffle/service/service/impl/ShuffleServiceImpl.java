package org.example.shuffle.service.service.impl;


import lombok.extern.log4j.Log4j2;
import org.example.shuffle.service.service.ShuffleService;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

@Log4j2
@Service
class ShuffleServiceImpl implements ShuffleService {
    @Override
    public List<Integer> generateShuffleArray(final Integer arraySize) {
        log.debug("generating shuffled array with size {}", arraySize);

        final int[] nonShuffledArray = IntStream.rangeClosed(1, arraySize)
                .toArray();

        for (int i = arraySize - 1; i > 0; i--) {
            int j = ThreadLocalRandom.current().nextInt(i+1);

            int temp = nonShuffledArray[i];
            nonShuffledArray[i] = nonShuffledArray[j];
            nonShuffledArray[j] = temp;
        }


        List<Integer> shuffledList =  Arrays.stream(nonShuffledArray)
                .boxed()
                .toList();

        log.debug("generated shuffled array with size {}", arraySize);
        return shuffledList;
    }
}
