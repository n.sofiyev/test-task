package org.example.shuffle.service.service.impl;

import lombok.extern.log4j.Log4j2;
import org.example.log.api.DefaultApi;
import org.example.log.api.model.LogPost201Response;
import org.example.log.api.model.LogPostRequest;
import org.example.shuffle.service.service.LogService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

@Log4j2
@Service
class LogServiceImpl implements LogService {

    private final ExecutorService logApiClientExecutor;
    private final Integer executorShutdownTimeout;
    private final DefaultApi defaultApi;


    LogServiceImpl(DefaultApi defaultApi,
                   ExecutorService logApiClientExecutor,
                   @Value("${apiExecutor.shutdown.timeout}") Integer executorShutdownTimeout) {
        this.executorShutdownTimeout = executorShutdownTimeout;
        this.logApiClientExecutor = logApiClientExecutor;
        this.defaultApi = defaultApi;
    }

    @Override
    public void postLogRequest(final String body) {
        logApiClientExecutor.submit(() -> {
            try {
                log.debug("Will send to logService, request: " + body);

                LogPostRequest logPostRequest = new LogPostRequest();
                logPostRequest.setRequest(body);

                LogPost201Response logPost201Response = defaultApi.logPost(logPostRequest);
                log.debug("Response from log service: " + logPost201Response);
            } catch (Exception e) {
                log.error("Error during executing http request", e);
            }
        });

    }

    @PreDestroy
    public void preDestroy() {
        logApiClientExecutor.shutdown();
        try {
            log.debug("Terminating logApiClientExecutor");
            if (!logApiClientExecutor.awaitTermination(executorShutdownTimeout, TimeUnit.SECONDS)) {
                log.debug("Will not wait for unExecutedTasks, terminating logApiClientExecutor now");
                List<Runnable> unExecutedTasks = logApiClientExecutor.shutdownNow();
                log.debug("Number of unExecutedTasks (Requests that not send to Log Service)" + unExecutedTasks.size());
                if (!logApiClientExecutor.awaitTermination(executorShutdownTimeout, TimeUnit.SECONDS)) {
                    log.error("Could not terminate logApiClientExecutor");
                }
            }
        } catch (InterruptedException ie) {
            logApiClientExecutor.shutdownNow();
            Thread.currentThread().interrupt();
        }
    }
}
