package org.example.shuffle.service.service;

import java.util.List;

public interface ShuffleService {

    List<Integer> generateShuffleArray(Integer arraySize);
}
