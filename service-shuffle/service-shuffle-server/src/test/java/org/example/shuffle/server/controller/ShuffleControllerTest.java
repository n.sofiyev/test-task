package org.example.shuffle.server.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.shuffle.api.model.ShuffleRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class ShuffleControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void test200Response() throws Exception {
        ShuffleRequest request = new ShuffleRequest();
        request.setNumber(10);

        mockMvc.perform(MockMvcRequestBuilders.post("/shuffle")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    public void test400ResponseWithBigValue() throws Exception {
        ShuffleRequest request = new ShuffleRequest();
        request.setNumber(2433);

        mockMvc.perform(MockMvcRequestBuilders.post("/shuffle")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.error").value("number: must be less than or equal to 1000"));
    }

    @Test
    public void test400ResponseWithLessValue() throws Exception {
        ShuffleRequest request = new ShuffleRequest();
        request.setNumber(0);

        mockMvc.perform(MockMvcRequestBuilders.post("/shuffle")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.error").value("number: must be greater than or equal to 1"));
    }
}
