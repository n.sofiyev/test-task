package org.example.shuffle.server.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.example.shuffle.api.ShuffleApi;
import org.example.shuffle.api.model.ShuffleRequest;
import org.example.shuffle.api.model.ShuffledArrayResponse;
import org.example.shuffle.service.service.LogService;
import org.example.shuffle.service.service.ShuffleService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Log4j2
@RestController
@RequiredArgsConstructor
public class ShuffleController implements ShuffleApi {

    private final ShuffleService shuffleService;
    private final ObjectMapper objectMapper;
    private final LogService logService;

    @Override
    public ResponseEntity shufflePost(@Validated @RequestBody ShuffleRequest shuffleRequest) {
        log.debug("shufflePost started");

        try {
            //TODO, after making ShuffleRequest immutable. for better performance we can convert ShuffleRequest intp to string in executor service inside LogService
            logService.postLogRequest(objectMapper.writeValueAsString(shuffleRequest));
        } catch (JsonProcessingException e) {
            log.error("Can not convert request into json string");
        }

        final List<Integer> shuffledList = shuffleService.generateShuffleArray(shuffleRequest.getNumber());

        ShuffledArrayResponse shuffledArrayResponse = new ShuffledArrayResponse();
        shuffledArrayResponse.setShuffledArray(shuffledList);

        log.debug("shufflePost ended");
        return ResponseEntity.ok(shuffledArrayResponse);
    }
}
