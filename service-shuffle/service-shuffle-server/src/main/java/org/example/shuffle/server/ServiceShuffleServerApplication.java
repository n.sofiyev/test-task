package org.example.shuffle.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceShuffleServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServiceShuffleServerApplication.class, args);
    }
}