package org.example.shuffle.server.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"org.example.shuffle.server",
        "org.example.shuffle.service"})
public class AppConfig {
}
