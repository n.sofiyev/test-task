package org.example.shuffle.server.exception;

import org.example.shuffle.api.model.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@ControllerAdvice
public class GlobalExceptionHandler {

    private static final String NOT_READABLE_EXCEPTION = "Can not read request. Correct format example: { \"number\": 10 }";

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ErrorResponse> handleValidationExceptions(MethodArgumentNotValidException ex, NativeWebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse();

        Object[] details = ex.getDetailMessageArguments();
        String result = Arrays.stream(details)
                .map(Object::toString)
                .filter(str -> !str.isEmpty())     // Filter out empty strings
                .collect(Collectors.joining(", "));

        errorResponse.setError(result);

        Map<String, String> errors = new HashMap<>();
        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.put(error.getField(), error.getDefaultMessage());
        }

        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ErrorResponse> handleValidationExceptions(Exception ex, NativeWebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setError(NOT_READABLE_EXCEPTION);

        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }
}
